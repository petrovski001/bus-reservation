## Installation

After you cloned the project on your machine open **Package Manager Console** and enter this command: `Update-Package -reinstall` and hit enter.
After all dependencies download, open the **Web.config** file and set up your connection string for your database.
```xml
<connectionStrings>
    <add name="BusReservation" connectionString="Data Source=YOUR_DATABASE_CONNECTION_STRING;Initial Catalog=aspnet-BusReservation-20181031125155;Integrated Security=True" providerName="System.Data.SqlClient" />
  </connectionStrings>
  ```
  
  After this build and run the project solution. 
  
## ---- 

