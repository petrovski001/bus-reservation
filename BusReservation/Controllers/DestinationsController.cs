﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using BusReservation.Models;

namespace BusReservation.Controllers
{
    public class DestinationsController : Controller
    {
        private BusReservationContext db = new BusReservationContext();

        // GET: Destinations
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.Destinations.ToList());
        }

        // GET: Destinations/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Destination destination = db.Destinations.Find(id);
            Bus bus = db.Buses.Find(destination.BusId);
            destination.Bus = bus;
            destination.Tickets = db.Tickets.Where(ticket => ticket.DestinationId == destination.DestinationId).ToList<Ticket>();
            if (destination == null)
            {
                return HttpNotFound();
            }
            
            ViewModel des = new ViewModel();
            des.Destinations = destination;
            des.Tickets = destination.Tickets;
            List<SelectListItem> list = new List<SelectListItem>
            {
            new SelectListItem { Text = "--- Select ---", Value = 0.ToString() },
            new SelectListItem { Text = "One Way Price ("+ destination.PriceOneWay.ToString()+")" , Value = destination.PriceOneWay.ToString() },
            new SelectListItem { Text = "Two Way Price ("+ destination.PriceTwoWay.ToString()+")", Value = destination.PriceTwoWay.ToString()},
            new SelectListItem { Text = "Student Price ("+ destination.PriceStudent.ToString()+")", Value = destination.PriceStudent.ToString() },
            };
            ViewBag.ceni = list;
            ViewBag.destination = destination;
            return View(des);
        }

        // GET: Destinations/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var bus in db.Buses.ToList<Bus>())
            {
                list.Add(new SelectListItem() { Value = bus.BusId.ToString(), Text = bus.Name });
            }
            ViewBag.Busovi = list;
            //ViewBag.Avtobusi = db.Buses.ToList<Bus>();
            return View();
        }

        // POST: Destinations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DestinationId,From,To,PriceOneWay,PriceTwoWay,PriceStudent,BusId")] Destination destination)
        {
            if (ModelState.IsValid)
            {
                db.Destinations.Add(destination);
                //list<bus> buses = db.buses.tolist<bus>();
                //bus bus = buses.find(x => x.busid == destination.busid);
                //for(int i = 0; i < bus.numberofseats; i++)
                //{
                //    ticket ticket = new ticket();
                //    db.tickets.add(ticket);
                //}
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(destination);
        }

        // GET: Destinations/Edit/5
        [Authorize(Roles ="Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Destination destination = db.Destinations.Find(id);
            if (destination == null)
            {
                return HttpNotFound();
            }
            return View(destination);
        }

        // POST: Destinations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DestinationId,From,To,PriceOneWay,PriceTwoWay,PriceStudent")] Destination destination)
        {
            if (ModelState.IsValid)
            {
                db.Entry(destination).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(destination);
        }

        // GET: Destinations/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Destination destination = db.Destinations.Find(id);
            if (destination == null)
            {
                return HttpNotFound();
            }
            return View(destination);
        }

        // POST: Destinations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Destination destination = db.Destinations.Find(id);
            db.Destinations.Remove(destination);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
