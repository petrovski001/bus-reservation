﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BusReservation.Models
{
    public class BusReservationContext : DbContext
    {
        public DbSet<Destination> Destinations { get; set; }
        public DbSet<Bus> Buses { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        //public DbSet<ApplicationUser> Users { get; set; }

        public BusReservationContext() : base("BusReservation")
        {

        }

    }
}