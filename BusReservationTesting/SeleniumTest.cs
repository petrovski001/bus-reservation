﻿using System;
using BusReservation.Controllers;
using BusReservation.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace BusReservationTesting
{
    [TestClass]
    public class SeleniumTest
    {

        private DestinationsController obj;
        private BusReservationContext db;
        public ViewModel expected;
        private ChromeDriver driver;

        [TestInitialize]
        public void Init()
        {
            obj = new DestinationsController();
            db = new BusReservationContext();
            expected = new ViewModel();
            driver = new ChromeDriver();
        }

        [TestMethod]
        public void LoginTest()
        {
            string url = "http://localhost:50811/Account/Login";
            driver.Navigate().GoToUrl(url);
            driver.FindElement(By.Id("Email")).SendKeys("name@email.com");
            driver.FindElement(By.Id("Password")).SendKeys("Password123!");
            driver.FindElement(By.ClassName("btn-default")).Click();
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 15));
            driver.Close();
            driver.Dispose();
        }

        [TestMethod]
        public void RegisterTest()
        {
            string url = "http://localhost:50811/Account/Register";
            driver.Navigate().GoToUrl(url);
            driver.FindElement(By.Id("Email")).SendKeys("user@email.com");
            driver.FindElement(By.Id("Password")).SendKeys("Password123!");
            driver.FindElement(By.Id("ConfirmPassword")).SendKeys("Password123!");
            driver.FindElement(By.ClassName("btn-default")).Click();
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 15));
            driver.Close();
            driver.Dispose();
        }

        [TestMethod]
        public void NavigateToDestinationTest()
        {
            string url = "http://localhost:50811/Account/Login";
            driver.Navigate().GoToUrl(url);
            driver.FindElement(By.Id("Email")).SendKeys("name@email.com");
            driver.FindElement(By.Id("Password")).SendKeys("Password123!");
            driver.FindElement(By.ClassName("btn-default")).Click();
            driver.FindElement(By.XPath("/html/body/div[1]/div/div[2]/ul/li[2]/a")).Click();
            driver.FindElement(By.XPath("/html/body/div[2]/table/tbody/tr[2]/td[6]/a[2]")).Click();
            driver.FindElement(By.XPath("//*[@id=\"checkboxes\"]/label[48]")).Click();
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
            driver.FindElement(By.XPath("/html/body/div[2]/form/div/div[2]/div/input")).Click();
            wait = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
            driver.Close();
            driver.Dispose();
        }

        [TestMethod]
        public void NavigateToBoughtTicketsTest()
        {
            string url = "http://localhost:50811/Account/Login";
            driver.Navigate().GoToUrl(url);
            driver.FindElement(By.Id("Email")).SendKeys("name@email.com");
            driver.FindElement(By.Id("Password")).SendKeys("Password123!");
            driver.FindElement(By.ClassName("btn-default")).Click();
            driver.FindElement(By.XPath("/html/body/div[1]/div/div[2]/ul/li[4]/a")).Click();
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
            driver.Close();
            driver.Dispose();
        }
    }
}