﻿using BusReservation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using BusReservation.Models;

namespace BusReservation.Test.Controllers
{
    [TestClass]
    public class BussesControllerTest
    {
        public BusesController BusesController { get; set; }

        [TestInitialize]
        public void TestInitializer()
        {
            BusesController = new BusesController();
        }

        [TestMethod]
        public void IndexTest()
        {
            // Act
            ViewResult result = BusesController.Index() as ViewResult;
            ActionResult actionResult = BusesController.Index();
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DetailsBadIdTest()
        {
            HttpStatusCodeResult result = (HttpStatusCodeResult)BusesController.Details(null);

            Assert.AreNotEqual((Int32)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public void DetailsGoodIdTest()
        {
            ActionResult result = BusesController.Details(4);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void CreateTest()
        {
            ActionResult result = BusesController.Create();
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void EditBadIdTest()
        {
            int? id = null;
            HttpStatusCodeResult result = (HttpStatusCodeResult)BusesController.Edit(id);
            Assert.AreEqual((Int32)HttpStatusCode.BadRequest, result.StatusCode);
        }

        [TestMethod]
        public void EditGoodIdTest()
        {
            ActionResult result = BusesController.Edit(4);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }



        [TestMethod]
        public void DeleteConfirmedRedirectToActionBadIdTest()
        {
            var result = BusesController.DeleteConfirmed(-1) as RedirectToRouteResult;

            Assert.AreEqual("Create", result.RouteValues["action"].ToString());
        }

    }
}